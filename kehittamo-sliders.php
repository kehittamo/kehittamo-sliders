<?php
/*
  Plugin Name: Kehittämö Sliders
  Plugin URI: http://www.kehittamo.fi
  Description: Creates slideshow from chosen category images, Allows user to add single image with title and caption
  Version: 0.1.0
  Author: Kehittämö Oy - Janne Saarela, Jani Krunniniva
  Author URI: http://www.kehittamo.fi
  License: GPL2
*/

/*  Copyright 2014  JANNE SAARELA  (email : janne.saarela@kehittamo.fi)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
/*
  Flex slider properties:
  https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties
*/
namespace Kehittamo\Plugins\Sliders;

define( 'Kehittamo\Plugins\Sliders\PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'Kehittamo\Plugins\Sliders\PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'Kehittamo\Plugins\Sliders\PLUGIN_SLUG', 'kehittamo-sliders' );
define( 'Kehittamo\Plugins\Sliders\VERSION_NUMBER', '0.1.0' );


define('kehittamo_slider_thumbnail_xs_width', 230);
define('kehittamo_slider_thumbnail_xs_height', 130);
define('kehittamo_slider_thumbnail_sm_width', 360);
define('kehittamo_slider_thumbnail_sm_height', 270);
define('kehittamo_slider_thumbnail_md_width', 720);
define('kehittamo_slider_thumbnail_md_height', 540);
define('kehittamo_slider_thumbnail_lg_width', 1200);
define('kehittamo_slider_thumbnail_lg_height', 900);


class Load {

  /**
   * Initialize the plugin
   *
   * @since     0.1.0
   */
  function __construct() {
    // Load Plugin
    add_action( 'plugins_loaded', array( $this, 'plugins_loaded') );
  }

  /**
   * Loads the plugin
   *
   * @since    0.1.0
   */
  function plugins_loaded() {

    // Load Translateiton
    $this->load_textdomain();

    // Load Javascript
    add_action( 'wp_enqueue_scripts', array( $this, 'js' ) );

    // Load Styles
    add_action( 'init', array( $this, 'styles' ) );

    // Load admin scripts
    add_action('admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts') );

    // Load Widgets
    add_action('widgets_init', array( $this, 'register_widgets' ) );

    require_once( PLUGIN_PATH . '/kehittamo-sliders-functions.php');
  }

  /**
   * Load the plugin text domain for translation
   *
   * @since     0.1.0
   */
  function load_textdomain() {
    // TODO: Muuta nämä
    // # Category #kehittamo-sliders-category
    load_plugin_textdomain( PLUGIN_SLUG, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

    // # Diashow #kehittamo-sliders-diashow
    load_plugin_textdomain( 'adian-flexslider-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
  }

  /**
   * Loads and registers widgets
   * @since    0.1.0
   */
  function register_widgets(){
    // Include the widget file
    // Register the Widget

    require_once( PLUGIN_PATH . '/widgets/widget-category.php');  // old adian-content-slider-widget, but template from kehittamo-slideshow
    register_widget( 'Kehittamo\Plugins\Sliders\Widget_Category' );

    require_once( PLUGIN_PATH . '/widgets/widget-diashow.php');   // old adian-flexslider
    register_widget( 'Kehittamo\Plugins\Sliders\Widget_Diashow' );
  }

  /**
   * Loads Javascript files
   *
   * @since     0.1.0
   */
  function js(){
    // Woo flexSlider (http://www.woothemes.com/flexslider/)
    wp_register_script( 'flexslider', PLUGIN_URL . 'assets/flexslider/jquery.flexslider-min.js', array('jquery') );
    wp_enqueue_script('flexslider');

    // Load plugin script + Register plugin style

    // # Category
    //wp_enqueue_script( 'kehittamo-sliders-category', PLUGIN_URL .'assets/js/kehittamo-sliders-category.js', array('jquery'), VERSION_NUMBER, true );
    wp_register_script( 'kehittamo-sliders-category', PLUGIN_URL . 'assets/js/kehittamo-sliders-category.js', array('flexslider','jquery'), VERSION_NUMBER, true);
    wp_enqueue_script( 'kehittamo-sliders-category');

    // # Diashow
    wp_register_script( 'kehittamo-sliders-diashow', PLUGIN_URL . 'assets/js/kehittamo-sliders-diashow.js', array('flexslider','jquery') );
    wp_enqueue_script('kehittamo-sliders-diashow');
  }



  /**
   * Load Admin Javascript and Styles
   *
   * @since     0.1.0
   */
  function admin_enqueue_scripts(){
    if( is_admin() ) {

      // upload media
      if(function_exists('wp_enqueue_media')) {
        wp_enqueue_media();
      }
      else {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
      }

      // # Category
      // Load plugin admin script + CSS Styles
      wp_enqueue_script( PLUGIN_SLUG . '-category-admin-script', PLUGIN_URL .'assets/js/kehittamo-sliders-category-admin.js', array('jquery','underscore'), VERSION_NUMBER, true );
      wp_enqueue_style( PLUGIN_SLUG . '-category-admin-style', PLUGIN_URL . 'assets/css/kehittamo-sliders-category-admin.css', '', VERSION_NUMBER, false );


      // # Diashow
      // Register plugin style
      wp_register_script( 'kehittamo-sliders-diashow-admin-js', PLUGIN_URL . 'assets/js/kehittamo-sliders-diashow-admin.js', array('wp-color-picker') );
      wp_enqueue_script('kehittamo-sliders-diashow-admin-js');

      // Register plugin admin style
      wp_register_style( 'kehittamo-sliders-diashow-admin-css', PLUGIN_URL . 'assets/css/kehittamo-sliders-diashow-admin.css' );
      wp_enqueue_style('kehittamo-sliders-diashow-admin-css');

       // first check that $hook_suffix is appropriate for your admin page
      wp_enqueue_style( 'wp-color-picker' );
    }
  }


  /**
   * Loads CSS styles
   *
   * @since     0.1.0
   */
  function styles(){
    if( !is_admin() ) {
      // Bootstrap
      wp_register_style('bootstrap-styles', '//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css', array(), null, 'all');
      wp_enqueue_style('bootstrap-styles');
    }


    // Register flexslider style
    wp_register_style( 'flexslider', PLUGIN_URL . 'assets/flexslider/flexslider.css' );
    wp_enqueue_style('flexslider');
    wp_register_style( 'flexslider-override', PLUGIN_URL . 'assets/flexslider/flexslider-override.css' );
    wp_enqueue_style('flexslider-override');


    // # Category
    wp_enqueue_style( 'kehittamo-sliders-category', PLUGIN_URL . 'assets/css/kehittamo-sliders-category-widget.css', '', VERSION_NUMBER, false );
    wp_enqueue_style('kehittamo-sliders-category');


    // # Diashow
    wp_register_style( 'kehittamo-sliders-diashow', PLUGIN_URL . 'assets/css/kehittamo-sliders-diashow-widget.css', array('flexslider') );
    wp_enqueue_style('kehittamo-sliders-diashow');
  }

}

$kehittamo_sliders = new \Kehittamo\Plugins\Sliders\Load();
