/**
* Thanks to Thomas Griffin for his super useful example on Github
*
* https://github.com/thomasgriffin/New-Media-Image-Uploader
*/

function initColorPicker( widget ) {
  widget.find( '.color-picker' ).wpColorPicker( {
    change: _.throttle( function() { // For Customizer
      jQuery(this).trigger( 'change' );
    }, 3000 )
  });
}

function onFormUpdate( event, widget ) {
  initColorPicker( widget );
}

jQuery( document ).on( 'widget-added widget-updated', onFormUpdate );

jQuery(document).ready(function($){

  $( '#widgets-right .widget:has(.color-picker)' ).each( function () {
    initColorPicker( $( this ) );
  });

  // Prepare the variable that holds our custom media manager.
  var adian_flexslider_media_frame;
  var formlabel = 0;

  // Bind to our click event in order to open up the new media experience.
  $(document.body).on('click', '.adian-flexslider-uploader .button-select', function(e){

    // Prevent the default action from occuring.
    e.preventDefault();
    // Get our Parent element
    formlabel = jQuery(this).parent();

    // If the frame already exists, re-open it.
    if ( adian_flexslider_media_frame ) {
      adian_flexslider_media_frame.open();
      return;
    }

    adian_flexslider_media_frame = wp.media.frames.adian_flexslider_media_frame = wp.media({
      //Create our media frame
      className: 'media-frame adian-media-frame',
      frame: 'select', //Allow Select Only
      multiple: false, //Disallow Mulitple selections
      library: {
      type: 'image' //Only allow images
      }
    });

    adian_flexslider_media_frame.on('select', function(){
      // Grab our attachment selection and construct a JSON representation of the model.
      var media_attachment = adian_flexslider_media_frame.state().get('selection').first().toJSON();

      // Send the attachment URL to our custom input field via jQuery.
      formlabel.find('.image-id').val(media_attachment.id);
      formlabel.find('.image-preview').attr('src', media_attachment.url);

      $('.button-remove').removeClass('button-hidden');
    });

    // Now that everything has been set, let's open up the frame.
    adian_flexslider_media_frame.open();
  });


  // Bind to our click event in order to open up the new media experience.
  $(document.body).on('click', '.adian-flexslider-uploader .button-remove', function(e){
    // Prevent the default action from occuring.
    e.preventDefault();

    // Get our Parent element
    formlabel = jQuery(this).parent();

    // Send the attachment URL to our custom input field via jQuery.
    formlabel.find('.image-id').val('');
    formlabel.find('.image-preview').attr('src', '');
    $(this).addClass('button-hidden');
  });

});
