<?php


namespace Kehittamo\Plugins\Sliders;


class Kehittamo_Slider_Functions {

	public function __construct() {
	}


	// CREATE PICTURE ELEMENT
	public function get_widget_image( $image_id ) {

		// IMAGE SIZES
		$thumbnail_size = '';
		$thumbnail_xs_url = wp_get_attachment_image_src( $image_id, array(kehittamo_slider_thumbnail_xs_width,kehittamo_slider_thumbnail_xs_height) )[0];
		$thumbnail_sm_url = wp_get_attachment_image_src( $image_id, array(kehittamo_slider_thumbnail_xs_width,kehittamo_slider_thumbnail_sm_height) )[0];
		$thumbnail_md_url = wp_get_attachment_image_src( $image_id, array(kehittamo_slider_thumbnail_xs_width,kehittamo_slider_thumbnail_md_height) )[0];
		$thumbnail_lg_url = wp_get_attachment_image_src( $image_id, array(kehittamo_slider_thumbnail_xs_width,kehittamo_slider_thumbnail_lg_height) )[0];

		$picture_size_mobile = $thumbnail_md_url;
		$picture_size_mobile_retina = $thumbnail_lg_url;
		$picture_size_default = $thumbnail_lg_url;
		$picture_size_retina = $thumbnail_lg_url;
		$picture_size_big = $thumbnail_lg_url;
		$picture_size_big_retina = $thumbnail_lg_url;

		$image = '<picture class="'.$thumbnail_size.'">';
      $image .= '<source srcset="'. $picture_size_big . ', ' . $picture_size_big_retina .' 2x" media="(min-width: 1200px)"/>';
      $image .= '<source srcset="'. $picture_size_default . ', ' . $picture_size_retina .' 2x" media="(min-width: 768px)"/>';
      $image .= '<source srcset="'. $picture_size_mobile . ', ' . $picture_size_mobile_retina .' 2x"/>';
      $image .= '<img src="'. $picture_size_default .'"/>';
    $image .= '</picture>';


    return $image;
	}
}
