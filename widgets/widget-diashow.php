<?php

namespace Kehittamo\Plugins\Sliders;

use \WP_Widget;

/**
 * Adds Adian_youtube_Widget widget.
 *
 * diaesitys widgetti, jonka hallinnassa voi valita 1-3 kuvaa, tekstiä, linkkiä ja napin tekstiä.
 * - Tätä voisi laajentaa niin, että hallinnassa voi valita kiinteän otsikon, tekstin, button tekstin ja linkkiurlin, joka näytetän aina vaikka kuva vaihtuu taustalla, tai kuvakohtaisen, jolloin joka kuvalla on oma otsikon, tekstin, button tekstin ja linkkiurlin. Jos joku kentistä on tyhjä ei ko. elementtiä näytetä.
 *
 * @since    0.1.0
 */
class Widget_Diashow extends WP_Widget {

  const max_slides = 3;

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'Kehittamo_Sliders_Plugin_Diashow', /* Base ID */
      __( '!!! Kehittämö Diashow', PLUGIN_SLUG ), /* Name */
      array( 'description' => __( 'Responsive Slider Widget', PLUGIN_SLUG ) )
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   *
   * @since    0.1.0
   */
  public function widget( $args, $instance ) {
    isset ( $instance[ 'show_title' ] ) ? $show_title = esc_attr( $instance[ 'show_title' ] ) : $show_title = false;
    $title = apply_filters( 'widget_title', $instance['title'] );
    $url = esc_url($instance['url']);
    $subtitle = strip_tags( $instance[ 'subtitle' ] );
    $caption = esc_textarea( $instance[ 'caption' ] );
    $color = strip_tags( $instance[ 'color' ] );
    $bgcolor = strip_tags( $instance[ 'bgcolor' ] );
    $button_one_text = strip_tags( $instance[ 'button_one_text' ] );
    $button_one_url = esc_url($instance['button_one_url']);
    $button_two_text = strip_tags( $instance[ 'button_two_text' ] );
    $button_two_url = esc_url($instance['button_two_url']);

    for ($x = 1; $x <= self::max_slides; $x++):
      ${"title_".$x} = strip_tags($instance[ 'title_' . $x ]);
      ${"subtitle_".$x} = strip_tags($instance[ 'subtitle_' . $x ]);
      ${"caption_".$x} = strip_tags($instance[ 'caption_' . $x ]);
      ${"image_".$x} = intval($instance[ 'image_' . $x ]);
      ${"image_preview_".$x} = wp_get_attachment_image_src(${"image_".$x}, 'full');
      ${"button_one_text_".$x} = strip_tags($instance[ 'button_one_text_' . $x ]);
      ${"button_one_url_".$x} = esc_url($instance[ 'button_one_url_' . $x ]);
      ${"button_two_text_".$x} = strip_tags($instance[ 'button_two_text_' . $x ]);
      ${"button_two_url_".$x} = esc_url($instance[ 'button_two_url_' . $x ]);
    endfor;

    $opacity = strip_tags( $instance[ 'opacity' ] );

    echo $args['before_widget']; ?>

    <?php
      function hex2rgb($hex) {
       $hex = str_replace("#", "", $hex);
       if(strlen($hex) == 3) {
          $r = hexdec(substr($hex,0,1).substr($hex,0,1));
          $g = hexdec(substr($hex,1,1).substr($hex,1,1));
          $b = hexdec(substr($hex,2,1).substr($hex,2,1));
       } else {
          $r = hexdec(substr($hex,0,2));
          $g = hexdec(substr($hex,2,2));
          $b = hexdec(substr($hex,4,2));
       }
       $rgb = array($r, $g, $b);
       return implode(",", $rgb);
      }
    ?>

    <style type="text/css">
      .flexslider .main-text,
      .flexslider a.btn {
        color: <?php echo $color; ?>;
      }
      .flexslider a.btn:hover {
        color: rgba(<?php echo hex2rgb( $bgcolor ) . ',' . $opacity; ?>)!important;
      }
      .flextitle {
        color: <?php echo $color; ?>!important;
      }
      .flexslider .btn {
        border-color: <?php echo $color; ?>!important;
      }
      .flexslider_bg {
        background-color: rgba(<?php echo hex2rgb( $bgcolor ) . ',' . $opacity; ?>)!important;
      }
      .btn:hover {
        background-color: <?php echo $color; ?>!important;
      }
    </style>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
<!--        <div id="container" class="cf">-->

        <div class="flexslider kehittamo">
          <ul class="slides">
            <?php for ($x = 1; $x <= self::max_slides; $x++):
              if( ${"image_preview_".$x} ) : ?>
              <li>
              <?php if(!$show_title): ?>
                <div class="kehittamo-diashow-header-text">
                  <div class="flexslider_bg">
                    <div>
                    <?php
                      $tmp_title = ${"title_".$x};
                      $tmp_subtitle = ${"subtitle_".$x};
                      $tmp_caption = ${"caption_".$x};
                      $tmp_button_one_text = ${"button_one_text_".$x};
                      $tmp_button_one_url = ${"button_one_url_".$x};
                      $tmp_button_two_text = ${"button_two_text_".$x};
                      $tmp_button_two_url = ${"button_two_url_".$x};
                    ?>
                      <?php if( $tmp_title ) : ?>
                        <h1><?php echo $tmp_title; ?></h1>
                      <?php endif; ?>

                      <?php if( $tmp_subtitle ) : ?>
                        <h3><?php echo $tmp_subtitle; ?></h3>
                      <?php endif; ?>

                      <?php if( $tmp_caption ) : ?>
                        <p><?php echo $tmp_caption; ?></p>
                      <?php endif; ?>
                    </div>
                    <?php if( $tmp_button_one_text OR $tmp_button_two_text ) : ?>
                      <div class="kehittamo-registration">
                        <?php if( $tmp_button_one_text ) : ?>
                          <a class="btn btn-clear btn-sm btn-min-block" href="<?php echo $tmp_button_one_url; ?>"><?php echo $tmp_button_one_text; ?></a>
                        <?php endif; ?>
                        <?php if( $tmp_button_two_text ) : ?>
                          <a class="btn btn-clear btn-sm btn-min-block" href="<?php echo $tmp_button_two_url; ?>"><?php echo $tmp_button_two_text; ?></a>
                        <?php endif; ?>
                      </div>
                    <?php endif; ?>
                </div>
                  </div>
                <?php endif; ?>
                <img src="<?php echo ${"image_preview_".$x}[0]; ?>" />
              </li>
            <?php endif; endfor; ?>
          </ul>

          <?php if($show_title): ?>
          <div class="main-text">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 text-center">
              <div class="flexslider_bg">
                <?php if( $title ) : ?>
                  <h1><?php echo $title; ?></h1>
                <?php endif; ?>

                <?php if( $subtitle ) : ?>
                  <h3><?php echo $subtitle; ?></h3>
                <?php endif; ?>

                <?php if( $caption ) : ?>
                  <p><?php echo $caption; ?></p>
                <?php endif; ?>

                <?php if( $button_one_text OR $button_two_text ) : ?>
                  <div class="registration">
                    <?php if( $button_one_text ) : ?>
                      <a class="btn btn-clear btn-sm btn-min-block" href="<?php echo $button_one_url; ?>"><?php echo $button_one_text; ?></a>
                    <?php endif; ?>
                    <?php if( $button_two_text ) : ?>
                      <a class="btn btn-clear btn-sm btn-min-block" href="<?php echo $button_two_url; ?>"><?php echo $button_two_text; ?></a>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>

              </div>
            </div>
          </div>
          <?php endif; ?>
        </div>
<!--      </div> -->
    </div>
    <?php echo $args['after_widget'];
  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   *
   * @since    0.1.0
   */
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) {
      $title = $instance[ 'title' ];
    }
    $subtitle = $instance[ 'subtitle' ];
    $caption = $instance[ 'caption' ];
    $button_one_text = strip_tags($instance[ 'button_one_text' ]);
    $button_one_url = esc_url($instance[ 'button_one_url' ]);
    $button_two_text = strip_tags($instance[ 'button_two_text' ]);
    $button_two_url = esc_url($instance[ 'button_two_url' ]);
    $image = intval($instance[ 'image' ]);
    $image_preview = wp_get_attachment_image_src($image, 'full');
    $color = strip_tags($instance[ 'color' ]);
    $bgcolor = strip_tags($instance[ 'bgcolor' ]);
    $opacity = strip_tags($instance[ 'opacity' ]);
    isset ( $instance[ 'show_title' ] ) ? $show_title = esc_attr( $instance[ 'show_title' ] ) : $show_title = false;
    ?>

    <div class="adian-flexslider-uploader">
    <div class="tabs">
      <ul class="tab-links">
          <li class="active"><a href="#tab1">Default</a></li>
          <?php for ($x = 1; $x <= self::max_slides; $x++): ?>
            <li><a href="#tab<?php echo ($x+1); ?>">Image <?php echo $x; ?></a></li>
          <?php endfor; ?>
      </ul>

      <div class="tab-content">
        <?php /* DEFAULT */ ?>
        <div id="tab1" class="tab active">
          <p>
            <label for="<?php echo $this->get_field_id('show_title'); ?>"><?php _e( 'Override images tab fields?', PLUGIN_SLUG );?></label>
            <input id="<?php echo $this->get_field_id('show_title'); ?>" type="checkbox" name="<?php echo $this->get_field_name('show_title'); ?>" value="<?php echo $show_title;?>" <?php checked( $show_title, true, true );?> />
          </p>
          <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'adian-flexslider-widget' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
          </p>
          <p>
            <label for="<?php echo $this->get_field_id( 'subtitle' ); ?>"><?php _e( 'Subtitle:', 'adian-flexslider-widget' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'subtitle' ); ?>" name="<?php echo $this->get_field_name( 'subtitle' ); ?>" type="text" value="<?php echo esc_attr( $subtitle ); ?>">
          </p>
          <p>
            <label for="<?php echo $this->get_field_id( 'caption' ); ?>"><?php _e( 'Caption:', 'adian-flexslider-widget' ); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id( 'caption' ); ?>" name="<?php echo $this->get_field_name( 'caption' ); ?>"><?php echo esc_textarea( $caption ); ?></textarea>
          </p>
          <p>
            <label for="<?php echo $this->get_field_id( 'button_one_text' ); ?>"><?php _e( 'Button Text 1:', 'adian-flexslider-widget' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'button_one_text' ); ?>" name="<?php echo $this->get_field_name( 'button_one_text' ); ?>" type="text" value="<?php echo esc_attr( $button_one_text ); ?>">
          </p>
          <p>
            <label for="<?php echo $this->get_field_id( 'button_one_url' ); ?>"><?php _e( 'Button URL 1:', 'adian-flexslider-widget' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'button_one_url' ); ?>" name="<?php echo $this->get_field_name( 'button_one_url' ); ?>" type="text" value="<?php echo esc_attr( $button_one_url ); ?>">
          </p>
          <p>
            <label for="<?php echo $this->get_field_id( 'button_two_text' ); ?>"><?php _e( 'Button Text 2:', 'adian-flexslider-widget' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'button_two_text' ); ?>" name="<?php echo $this->get_field_name( 'button_two_text' ); ?>" type="text" value="<?php echo esc_attr( $button_two_text ); ?>">
          </p>
          <p>
            <label for="<?php echo $this->get_field_id( 'button_two_url' ); ?>"><?php _e( 'Button URL 2:', 'adian-flexslider-widget' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'button_two_url' ); ?>" name="<?php echo $this->get_field_name( 'button_two_url' ); ?>" type="text" value="<?php echo esc_attr( $button_two_url ); ?>">
          </p>
        </div>

        <?php /* IMAGE TABS */ ?>
        <?php for ($x = 1; $x <= self::max_slides; $x++): ?>
          <?php
            if ( isset( $instance[ 'title_' . $x ] ) ) {
              $title = $instance[ 'title_' . $x ];
            }
            $subtitle = $instance[ 'subtitle_'. $x ];
            $caption = $instance[ 'caption_'. $x ];
            $button_one_text = strip_tags($instance[ 'button_one_text_'. $x ]);
            $button_one_url = esc_url($instance[ 'button_one_url_'. $x ]);
            $button_two_text = strip_tags($instance[ 'button_two_text_'. $x ]);
            $button_two_url = esc_url($instance[ 'button_two_url_'. $x ]);
            $image = intval($instance[ 'image_'. $x ]);
            $image_preview = wp_get_attachment_image_src($image, 'full');
          ?>
          <div id="tab<?php echo ($x+1); ?>" class="tab">
            <p>
              <label for="<?php echo $this->get_field_id( 'title_'.$x ); ?>"><?php _e( 'Title:', 'adian-flexslider-widget' ); ?></label>
              <input class="widefat" id="<?php echo $this->get_field_id( 'title_'.$x ); ?>" name="<?php echo $this->get_field_name( 'title_'.$x ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
            </p>
            <p>
              <label for="<?php echo $this->get_field_id( 'subtitle_'.$x ); ?>"><?php _e( 'Subtitle:', 'adian-flexslider-widget' ); ?></label>
              <input class="widefat" id="<?php echo $this->get_field_id( 'subtitle_'.$x ); ?>" name="<?php echo $this->get_field_name( 'subtitle_'.$x ); ?>" type="text" value="<?php echo esc_attr( $subtitle ); ?>">
            </p>
            <p>
              <label for="<?php echo $this->get_field_id( 'caption_'.$x ); ?>"><?php _e( 'Caption:', 'adian-flexslider-widget' ); ?></label>
              <textarea class="widefat" id="<?php echo $this->get_field_id( 'caption_'.$x ); ?>" name="<?php echo $this->get_field_name( 'caption_'.$x ); ?>"><?php echo esc_textarea( $caption ); ?></textarea>
            </p>
            <p>
              <label for="<?php echo $this->get_field_id( 'image_'.$x ); ?>"><?php _e( 'Image:', 'adian-flexslider-widget' ); ?></label>
              <input class="image-id" id="<?php echo $this->get_field_id( 'image_'.$x ); ?>" name="<?php echo $this->get_field_name( 'image_'.$x ); ?>" type="hidden" value="<?php echo esc_attr( $image ); ?>">
              <img class="image-preview img-circle" src="<?php echo $image_preview[0]; ?>"/>
              <button class="button button-select"><?php _e( 'Select Image', 'adian-flexslider-widget' ); ?></button>
              <button class="button button-remove<?php if( ! $image_preview ) echo ' button-hidden'; ?>"><?php _e( 'Remove Image', 'adian-flexslider-widget' ); ?></button>
            </p>
            <p>
              <label for="<?php echo $this->get_field_id( 'button_one_text_'.$x ); ?>"><?php _e( 'Button Text 1:', 'adian-flexslider-widget' ); ?></label>
              <input class="widefat" id="<?php echo $this->get_field_id( 'button_one_text_'.$x ); ?>" name="<?php echo $this->get_field_name( 'button_one_text_'.$x ); ?>" type="text" value="<?php echo esc_attr( $button_one_text ); ?>">
            </p>
            <p>
              <label for="<?php echo $this->get_field_id( 'button_one_url_'.$x ); ?>"><?php _e( 'Button URL 1:', 'adian-flexslider-widget' ); ?></label>
              <input class="widefat" id="<?php echo $this->get_field_id( 'button_one_url_'.$x ); ?>" name="<?php echo $this->get_field_name( 'button_one_url_'.$x ); ?>" type="text" value="<?php echo esc_attr( $button_one_url ); ?>">
            </p>
            <p>
              <label for="<?php echo $this->get_field_id( 'button_two_text_'.$x ); ?>"><?php _e( 'Button Text 2:', 'adian-flexslider-widget' ); ?></label>
              <input class="widefat" id="<?php echo $this->get_field_id( 'button_two_text_'.$x ); ?>" name="<?php echo $this->get_field_name( 'button_two_text_'.$x ); ?>" type="text" value="<?php echo esc_attr( $button_two_text ); ?>">
            </p>
            <p>
              <label for="<?php echo $this->get_field_id( 'button_two_url_'.$x ); ?>"><?php _e( 'Button URL 2:', 'adian-flexslider-widget' ); ?></label>
              <input class="widefat" id="<?php echo $this->get_field_id( 'button_two_url_'.$x ); ?>" name="<?php echo $this->get_field_name( 'button_two_url_'.$x ); ?>" type="text" value="<?php echo esc_attr( $button_two_url ); ?>">
            </p>
          </div>
          <?php endfor; ?>
        </div>
      </div>

      <p>
        <label for="<?php echo $this->get_field_id( 'color' ); ?>"><?php _e( 'Text Color:', 'adian-flexslider-widget' ); ?></label><br />
        <input class="color-picker widefat" id="<?php echo $this->get_field_id( 'color' ); ?>" name="<?php echo $this->get_field_name( 'color' ); ?>" type="text" value="<?php echo esc_attr( $color ); ?>" data-default-color="#ffffff">
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'bgcolor' ); ?>"><?php _e( 'Background color:', 'adian-flexslider-widget' ); ?></label><br />
        <input class="color-picker widefat" id="<?php echo $this->get_field_id( 'bgcolor' ); ?>" name="<?php echo $this->get_field_name( 'bgcolor' ); ?>" type="text" value="<?php echo esc_attr( $bgcolor ); ?>" data-default-color="#ffffff">
      </p>
      <p>
        <label for="<?php echo $this->get_field_id( 'opacity' ); ?>"><?php _e( 'Background opacity:', 'adian-flexslider-widget' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'opacity' ); ?>" name="<?php echo $this->get_field_name( 'opacity' ); ?>" type="range" min="0" max="1" step="0.01" value="<?php echo esc_attr( $opacity ); ?>">
      </p>

    </div>
    <script>
      jQuery(document).ready(function() {
        jQuery('.tabs .tab-links a').on('click', function(e)  {
          var currentAttrValue = jQuery(this).attr('href');

          // Show/Hide Tabs
          jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

          // Change/remove current tab to active
          jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

          e.preventDefault();
        });
      });
    </script>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   *
   * @since    0.1.0
   */
  public function update( $new_instance, $old_instance ) {

    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['subtitle'] = ( ! empty( $new_instance['subtitle'] ) ) ? strip_tags( $new_instance['subtitle'] ) : '';
    $instance['caption'] = ( ! empty( $new_instance['caption'] ) ) ? esc_textarea( $new_instance['caption'] ) : '';
    $instance['button_one_text'] = ( ! empty( $new_instance['button_one_text'] ) ) ? strip_tags( $new_instance['button_one_text'] ) : '';
    $instance['button_one_url'] = ( ! empty( $new_instance['button_one_url'] ) ) ? esc_url( $new_instance['button_one_url'] ) : '';
    $instance['button_two_text'] = ( ! empty( $new_instance['button_two_text'] ) ) ? strip_tags( $new_instance['button_two_text'] ) : '';
    $instance['button_two_url'] = ( ! empty( $new_instance['button_two_url'] ) ) ? esc_url( $new_instance['button_two_url'] ) : '';
    $instance['image'] = ( ! empty( $new_instance['image'] ) ) ? intval( $new_instance['image'] ) : '';
    $instance['color'] = ( ! empty( $new_instance['color'] ) ) ? strip_tags( $new_instance['color'] ) : '';
    $instance['bgcolor'] = ( ! empty( $new_instance['bgcolor'] ) ) ? strip_tags( $new_instance['bgcolor'] ) : '';
    $instance['opacity'] = ( ! empty( $new_instance['opacity'] ) ) ? strip_tags( $new_instance['opacity'] ) : '';
    isset ( $new_instance[ 'show_title' ] ) ? $instance[ 'show_title' ] = true : $instance['show_title' ] = false;

    for ($x = 1; $x <= self::max_slides; $x++):
      $instance['title_'.$x] = ( ! empty( $new_instance['title_'.$x] ) ) ? strip_tags( $new_instance['title_'.$x] ) : '';
      $instance['subtitle_'.$x] = ( ! empty( $new_instance['subtitle_'.$x] ) ) ? strip_tags( $new_instance['subtitle_'.$x] ) : '';
      $instance['caption_'.$x] = ( ! empty( $new_instance['caption_'.$x] ) ) ? esc_textarea( $new_instance['caption_'.$x] ) : '';

      $instance['button_one_text_'.$x] = ( ! empty( $new_instance['button_one_text_'.$x] ) ) ? strip_tags( $new_instance['button_one_text_'.$x] ) : '';
      $instance['button_one_url_'.$x] = ( ! empty( $new_instance['button_one_url_'.$x] ) ) ? esc_url( $new_instance['button_one_url_'.$x] ) : '';
      $instance['button_two_text_'.$x] = ( ! empty( $new_instance['button_two_text_'.$x] ) ) ? strip_tags( $new_instance['button_two_text_'.$x] ) : '';
      $instance['button_two_url_'.$x] = ( ! empty( $new_instance['button_two_url_'.$x] ) ) ? esc_url( $new_instance['button_two_url_'.$x] ) : '';

      $instance['image_'.$x] = ( ! empty( $new_instance['image_'.$x] ) ) ? intval( $new_instance['image_'.$x] ) : '';
    endfor;

    return $instance;
  }

} // class Widget_Diashow
