<?php

namespace Kehittamo\Plugins\Sliders;

use \WP_Widget;

/**
 * NOTES :
 * https://premium.wpmudev.org/blog/how-to-get-perfect-blog-images-every-time-with-auto-scale-and-crop-for-wordpress/
*
*
*/
/**
 * Kehittamo_Sliders_Widget_Content_Slider Class
 *
 * - näyttää halutun kategorian artikkelikuvat diaesityksenä
 * - kuinka monta kuvaa näytetään kerralla
 * - kuinka monta kuvaa on sarjassa
 * - näytetäänkö otsikko vai ei.
 *
 * @since    0.1.0
 */
class Widget_Category extends WP_Widget {
  /** constructor */
  function __construct() {
    parent::WP_Widget(
      'Kehittamo_Sliders_Plugin_Category', /* Base ID */
      __( '!!! Kehittämö Category', PLUGIN_SLUG ), /* Name */
      array( 'description' => __( 'Creates a sliders from chosen category post attachments', PLUGIN_SLUG ) )
    );
  }

  /**
   * @see WP_Widget::widget
   *
   * @since    0.1.0
   */
  function widget( $args, $instance ) {
    extract( $args );

    $show_title = $instance[ 'show_title' ];
    $sliders_id = $instance[ 'sliders_id' ];
    $title = $instance[ 'title' ];
    $images_max = $instance[ 'images_max' ];
    $images_at_time = $instance[ 'images_at_time' ];

    global $post;

    $all_data = get_transient( 'kehittamo_sliders_post_data_' . $sliders_id );

    if( $instance['category_id'] != -1 ) $category_id = $instance['category_id'];
    else $category_id = false;

    if ( !$all_data && $category_id ) :

      $options = array(
        'posts_per_page'   => $images_max,
        'category'         => $category_id,
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'post_type'        => 'post',
        'post_status'      => 'publish'
      );
      $posts_array =  get_posts( $options );

      $count = count($posts);

      $all_data = array();
      $i = 0;

      foreach ( $posts_array as $post ) :
        setup_postdata( $post );
        $item = get_children( array( 'numberposts' => 1, 'post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image' ) );

        if ( !empty( $item ) ) {
          $thumb_id = get_post_thumbnail_id(get_the_ID());
          if( $thumb_id ) {
            $all_data[$i]['ID'] = get_the_ID();
            $all_data[$i]['post_title'] = get_the_title();
            $all_data[$i]['post_excerpt'] = get_the_excerpt();
            $all_data[$i]['post_permalink'] = get_permalink();

            $all_data[$i]['picture'] = Kehittamo_Slider_Functions::get_widget_image($thumb_id);
          }
          $i++;
        }
      endforeach;

      set_transient( 'kehittamo_sliders_post_data_' . $sliders_id, $all_data, 0 );
      wp_reset_postdata();
    endif;

    if( count($all_data) < intval($images_at_time) ) {
      $images_at_time = count($all_data);
    }

    echo $before_widget;

    if ( $all_data && $category_id ) : ?>
    <style>
     /*FOR DEVELOPING*/
     /*
      .widget {
        margin:0;
        padding:0;
      }
      */
    </style>
    <script>
        (function($) {
          $(document).ready(function() {
            var slide_area_width = $('.kehittamo-sliders-category').width();
            var itemWidth = (slide_area_width / <?php echo $images_at_time; ?> );
            $('.kehittamo-sliders-category').flexslider({
              animation: 'slide',
              smoothHeight: false,
              itemWidth: itemWidth,
              itemMargin: <?php echo $images_at_time; ?>
            });
          });
        })(jQuery);
        </script>
      <div class="row kehittamo-sliders">
        <div class="col-xs-12 col-sm-12 col-md-12">
          <div class="kehittamo-sliders-category flexslider">
            <?php if ( $show_title ) : ?>
              <div class="slides_title"><?php echo $title; ?></div>
            <?php endif; ?>
            <ul class="slides">
                <?php
                  foreach ($all_data as $post) : ?>
                    <li><?php echo $post['picture']; ?></li>
                  <?php
                  endforeach;
                ?>
            </ul>
          </div>
        </div>
      </div>

  <?php endif; ?>


    <?php echo $after_widget;
  }

  /**
   * @see WP_Widget::update
   *
   * @since    0.1.0
   */
  function update( $new_instance, $old_instance ) {
    $instance = $old_instance;

    $instance[ 'sliders_id' ] = strip_tags( $this->id );
    $instance[ 'category_id' ] = strip_tags( $new_instance[ 'category_id' ] );
    isset ( $new_instance[ 'show_title' ] ) ? $instance[ 'show_title' ] = true : $instance['show_title' ] = false;

    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['images_at_time'] = ( ! empty( $new_instance['images_at_time'] ) ) ? strip_tags( $new_instance['images_at_time'] ) : '';
    $instance['images_max'] = ( ! empty( $new_instance['images_max'] ) ) ? strip_tags( $new_instance['images_max'] ) : '';
/*
    isset ( $new_instance[ 'show_excerpt' ] ) ? $instance[ 'show_excerpt' ] = true : $instance['show_excerpt' ] = false;

    if ( intval( $new_instance[ 'image_link'] ) == 2 && $new_instance[ 'images_are_hyperlinks_to' ] == '') {
      $instance['image_link'] = 1;
      $instance[ 'images_are_hyperlinks_to' ] = '';
    } else {
      $instance[ 'images_are_hyperlinks_to' ] = esc_url( $new_instance[ 'images_are_hyperlinks_to' ] );
      $instance['image_link'] =  intval( $new_instance[ 'image_link'] );
    }
*/
    delete_transient( 'kehittamo_sliders_post_data_' . $instance[ 'sliders_id' ] );

    return $instance;
  }

  /**
   * @see WP_Widget::form
   *
   * @since    0.1.0
   */
  function form( $instance ) {
    isset ( $instance[ 'category_id' ] ) ? $category_id = esc_attr( $instance[ 'category_id' ] ) : $category_id = -1;
    isset ( $instance[ 'show_title' ] ) ? $show_title = esc_attr( $instance[ 'show_title' ] ) : $show_title = false;

    $title = $instance[ 'title' ];
    $images_at_time = $instance[ 'images_at_time' ];
    $images_max = $instance[ 'images_max' ];

    ?>
    <p>
      <label for="<?php echo $this->get_field_id('category_id'); ?>"><?php _e('Post category:', PLUGIN_SLUG ); ?></label>
      <?php wp_dropdown_categories( array( 'hide_empty' => 1, 'name' => $this->get_field_name('category_id'), 'id' => $this->get_field_id('category_id'), 'orderby' => 'name', 'selected' => $category_id, 'hierarchical' => true, 'show_option_none' => __('None'), PLUGIN_SLUG )); ?>
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('show_title'); ?>"><?php _e( 'Show title?', PLUGIN_SLUG );?></label>
      <input id="<?php echo $this->get_field_id('show_title'); ?>" type="checkbox" name="<?php echo $this->get_field_name('show_title'); ?>" value="<?php echo $show_title;?>" <?php checked( $show_title, true, true );?> />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', PLUGIN_SLUG ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>

    <p>
      <label for="<?php echo $this->get_field_id( 'images_at_time' ); ?>"><?php _e( 'How many images at one slide:', PLUGIN_SLUG ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'images_at_time' ); ?>" name="<?php echo $this->get_field_name( 'images_at_time' ); ?>" type="text" value="<?php echo esc_attr( $images_at_time ); ?>">
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'images_max' ); ?>"><?php _e( 'How many images max:', PLUGIN_SLUG ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'images_max' ); ?>" name="<?php echo $this->get_field_name( 'images_max' ); ?>" type="text" value="<?php echo esc_attr( $images_max ); ?>">
    </p>


    <?php
  }

}
